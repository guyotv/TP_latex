%\documentclass[11pt,a4paper]{article}
\documentclass[12pt,a4paper,twoside]{scrartcl} % scrartcl est la classe d'articles de Koma-script
% qui gère nativement le format A4 ; utilisez article pour un format de papier américain
\usepackage[utf8]{inputenc} % gestion d'utf8
\usepackage[francais]{babel} % gestion de la langue française
\usepackage[T1]{fontenc} % 
\usepackage{amsmath}	% symboles mathématiques
\usepackage{amsfonts} % fontes pour les symboles mathématiques
\usepackage{amssymb} % jeu de symboles suppélémentaires
\usepackage{graphicx} % pour mettre des images
\usepackage{caption} % pour des légendes
\usepackage{lmodern}	% fonte spéciale
\usepackage{subcaption} % pour les subfigures et les subtables
\usepackage{float} % pour placer exactement les figure (utilisation de H)
\usepackage{url} % pour une bonne gestion des urls
\usepackage{siunitx} % pour la gestion des unités
\usepackage{latexsym}
\usepackage{keyval}
\usepackage{ifthen} % pour permettre des tests (avec gnuplot)
\usepackage{moreverb}
\usepackage[siunitx]{gnuplottex} % pour permettre l'utilisation de gnuplot avec siunitx
\graphicspath{{./images/}} % nécessaire pour l'import des fichiers eps_tex dans un sous répertoire

\author{Nom des auteurs, classe}
\title{Titre explicite du travail pratique}

\begin{document}
\maketitle
\renewcommand{\tablename}{Tableau}

\section{But}
Placez ici le but de l'expérience en une ou deux lignes.

\medskip
Par exemple, pour l'expérience du pendule, on pourrait écrire :

\smallskip
Pour l'étude d'un pendule simple, nous avons retenu la période de celui-ci comme grandeur accessible et facile à mesurer. Parmi les nombreux paramètres dont elle pourrait dépendre, il nous a semblé que la masse, la longueur et l'angle initial du pendule devaient être les plus importants. Comme il sont facilement mesurable, nous avons décidé de découvrir leur influence sur la période.

\section{Théorie}
On place ici les théories en rapport avec l'expérience réalisées. On peut aussi placer les équations nécessaires à déterminer les grandeurs expérimentales dérivées des mesures brutes.

\medskip
Par exemple, toujours pour le pendule simple, on pourrait dire qu'une première réflexion nous a amené à penser que seul l'angle n'est probablement pas un paramètre influençant la période.

Puis, qu'après des recherches sur internet, deux équations permettant de déterminer la période uniquement en fonction de la longueur sont souvent présentées. La première, l'équation \ref{equation:premiere}, est la plus présente, mais l'équation \ref{equation:seconde} l'est aussi et il se trouve même un cas où les deux sont simultanément présentes, sans plus d'explications.
\begin{align}
T=2\cdot \pi\cdot \sqrt{\frac{L}{g}} \label{equation:premiere}\\
T=2\cdot \pi\cdot\frac{L}{g} \label{equation:seconde}
\end{align}

Les équations \ref{equation:premiere} et \ref{equation:seconde} sont donc à considérer comme théories possibles.

\medskip
Quant à nous, nous avons développé une théorie qui mène à l'équation \ref{equation:laperso}, qui sera donc aussi à vérifier.
\begin{equation}\label{equation:laperso}
T=2\cdot \frac{L}{m}
\end{equation}
Nous avons pourtant hésité avec une variante de l'équation \label{equation:perso} ou la période augmente avec la masse \(T=2\cdot L\cdot m\).

\medskip
Remarquez que techniquement pour mettre une équation unique hors ligne avec un numéro de référence il faut utiliser l'environnement \verb|equation|
\begin{verbatim}
\begin{equation}
equation
\end{equation}
\end{verbatim}
alors que si on ne désire pas de numérotation, il faut utiliser l'environnement étoilé \verb|equation*|, qui est équivalent à la notation \verb|\[equation\]|. Pour une équation en ligne (à l'intérieur d'une ligne), il faut utiliser la notation \verb|\(equation\)|.

Enfin, sur le net, on trouve les notation \verb|$equation$| et \verb|$$equation$$| pour des équations respectivement en ligne et hors ligne, mais celles-ci sont dépréciées et il ne faut plus les utiliser.

\section{Dispositif expérimental}
Placez ici la description de l'expérience (une méthode et non une procédure) et le schéma du dispositif expérimental. La figure \ref{figure:dispos2} en donne un exemple (image flottante, c'est normal !). La méthode comprend une description du dispositif, de la manière de réaliser  les mesures en fonction des paramètres choisis et des conditions spéciales auxquelles il faut faire attention (comme une vitesse initiale nulle par exemple : le lâcher du pendule).

Remarquez que le schéma du dispositif expérimental (Figure \ref{figure:dispos2}) doit être réalisé avec \emph{inkscape}, qu'il faut l’enregistrer dans le répertoire \emph{FichiersDivers} au format .svg. Puis, quand il est terminé, il doit être \emph{Enregistré comme copie} au format .eps. En effet, le format svg permet des modifications (voyez cela comme le format de fichier natif d'un logiciel comme Gimp, incluant tous les caques et l'historique des modifications), alors que eps est une image vectorielle indépendante du facteur d'échelle, mais difficilement modifiable (comme une image pdf). Enfin, il faut effectuer une opération décrite dans l'annexe \ref{annexe:schema}, pour que les fontes \LaTeX{} soient utilisées directement dans le schéma.

\begin{figure}[ht]
\caption[Rail]{Le rail horizontal}\label{figure:dispos2}
\smallskip
\centering
%\def\svgwidth{0.7\columnwidth}
\def\svgwidth{10cm}
\input{./images/RailHorizontal2.eps_tex}
\end{figure}

\section{Résultats}
\subsection{Résultats bruts}

Les figures \ref{figure:periode_masse} et \ref{pic:latex} donnent un exemple de graphe. Relevez les éléments qui le composent et qui sont nécessaires. Relevez aussi la manière de mettre une ligne de sous-titre pour préciser les conditions de la mesure. Relevez aussi la présence des unités sur les axes. Tout cela est absolument indispensable.

La figure \ref{figure:periode_masse} est un graphe réalisé sous Gnumeric et exporté en eps. La figure \ref{pic:latex} est un graphe réalisé à l'intérieur de \LaTeX{} directement avec Gnuplot. Le fichier de mesures se trouve dans le même répertoire que le présent modèle de TP. La réalisation de ce type de graphique est décrite à l'annexe \ref{annexe:gnuplot}

Si le graphe est réalisé avec Gnumeric, il faut faire attention à conserver la feuille de calcul à partir de laquelle il a été réalisé ainsi que le graphe lui-même en l'enregistrant dans le répertoire \emph{FichiersDivers} au format .gnumeric. Par ailleurs, il faut enregistrer le graphe au format .eps. Cela permettra de l'inclure à votre document \LaTeX{} et de l'agrandir si nécessaire sans aucune perte de qualité.

\begin{figure}
\centering
\includegraphics[scale=0.6]{images/GraphePeriodeMasse.eps} 
\caption[Période vs masse]{Le pendule simple\par \small Période vs masse (L~=~\SI{1}{\metre} ; \(\alpha = \SI{30}{\degres}\))}\label{figure:periode_masse}
\end{figure}

\begin{figure}%
\centering%
\begin{gnuplot}[terminal=epslatex, terminaloptions=color dashed]
set key bottom left Left
set key width 1.5
set sample 1000
set xr [0:110]
set yr [0:20]
set xlabel 'Masse (\si{\gram})' # attention il faut parfois utiliser certains guillemets
set ylabel "Période (puls)"
#plot test.txt w l lc 1 t "$\sin(x)$",cos(x) w l lc 2 t "$\cos(x)$",tan(x) w l lc 3 t "$\tan(x)$",tanh(x) w l lc 4 t "$\tanh(x)$"
plot 'test.txt'\
 index 2 u 1:2:3 title 'Petites masses' w yerrorbars pt 4
\end{gnuplot}
\caption[Période vs masse]{Le pendule simple\par \small Période vs masse (L~=~\SI{1}{\metre} ; \(\alpha = \SI{30}{\degres}\))}%
\label{pic:latex}%
\end{figure}%

\medskip
La table \ref{tableau:accelerations} donne un exemple de résultats sous forme de tableau. Elle est issu d'un tableau réalisé sous \emph{Gnumeric}, puis exporté par \emph{Exporter les données} du menu \emph{Données}, en enregistrant un fichier de format \emph{Fragment de table \LaTeX} dans Gnumeric, en en copiant la table et en l'insérant dans un environnement de tableau \verb|tabular| dont on peut voir la structure dans le code du tableau \ref{tableau:accelerations}.

\medskip
Généralement on ne met pas de tableau mais des graphes. Mais cela peut parfois être utile pour résumer un ensemble de mesures à l'aide de plusieurs moyennes par exemple. C'est pourquoi, il vous sera demandé de le faire tout de même.

\begin{table}
\centering
\caption[Accélérations]{\centering Accélérations et écarts\par \small Autres paramètres : m~=~\SI{50}{\gram} ; L~=~\SI{50}{\centi\metre}}\label{tableau:accelerations}
\begin{tabular}{|c|c|}
\hline
\(\alpha\) & T \\
\si{\degres} & puls \\ \hline
10 & 20 \\
20 & 18 \\
30 & \dots \\ \hline
\end{tabular}
\end{table}

Le tableau \ref{tableau:multiples} est un exemple de placement de tableaux côte à côte, avec pour le tableau \ref{tableau:periode-masse} la période en fonction de la masse, pour le tableau \ref{tableau:meriode-angle} la période en fonction de l'angle et pour le tableau \ref{tableau:periode-longueur}.

\begin{table}
 \centering
 \begin{subtable}[b]{0.3\textwidth}
  \centering
  \begin{tabular}{|c|c|}
   \hline
   m & T \\
   \si{\gram} & puls \\ \hline
   10 & 20 \\
   20 & 18 \\
   30 & \dots \\ \hline
  \end{tabular}
  \caption[T vs m]{\centering Période vs masse\par \small Autres paramètres : \(\alpha=\SI{50}{\degres}\) ; L~=~\SI{50}{\centi\metre}}\label{tableau:periode-masse}
 \end{subtable}
 \begin{subtable}[b]{0.3\textwidth}
  \centering
  \begin{tabular}{|c|c|}
   \hline
   \(\alpha\) & T \\
   \si{\degres} & puls \\ \hline
   10 & 20 \\
   20 & 18 \\
   30 & \dots \\ \hline
  \end{tabular}
  \caption[T vs \(\alpha\)]{\centering Période vs angle\par \small Autres paramètres : m~=~\SI{50}{\gram} ; L~=~\SI{50}{\centi\metre}}\label{tableau:meriode-angle}
 \end{subtable}
  \begin{subtable}[b]{0.3\textwidth}
  \centering
  \begin{tabular}{|c|c|}
   \hline
   L & T \\
   \si{\centi\metre} & puls \\ \hline
   10 & 20 \\
   20 & 18 \\
   30 & \dots \\ \hline
  \end{tabular}
  \caption[T vs L]{\centering Période vs longueur\par \small Autres paramètres : m~=~\SI{50}{\gram} ; \(\alpha=\SI{50}{\degres}\)}\label{tableau:periode-longueur}
 \end{subtable}
  \caption{Un environnement pour de multiples tableaux}\label{tableau:multiples}
\end{table}

\subsection{Exemples de calcul}

Lorsque des grandeurs calculées, à l'exception des grandeurs brutes, sont présentes, il est nécessaire de présenter un exemple de calcul pour celles-ci en se référant à une ligne du tableau présenté. Ce n'est pas le cas pour le pendule simple, puisque nous avons utilisé le pouls comme instrument de mesure de la période. L'exemple suivant est donc totalement fictif.

Les exemples de calculs ci-dessous sont donnés pour la première ligne du tableau \ref{tableau:accelerations} et se basent sur les équations \ref{equation:premiere}, \ref{equation:seconde} et \ref{equation:laperso} données dans la théorie.

\begin{align*}
T&=2\cdot \pi\cdot \sqrt{\frac{L}{g}}=2\cdot \pi\cdot\sqrt{\frac{1}{9,81}}=\SI{2,006}{\second}\\
T&=2\cdot \pi\cdot \frac{L}{g}=2\cdot \pi\cdot\frac{1}{9,81}=\SI{0.641}{\second}\\
T&=2\cdot \frac{L}{m}=2\cdot \frac{1}{0,010}=\SI{200}{\second}
\end{align*}

\section{Discussion}
\subsection{Présentation des résultats}
En résumé, il n'y a pas de méthode permettant de faire une bonne discussion des résultats. Il est cependant important de bien montrer que vous avez tenté d'aller au-delà des chiffres pour tenter de les comprendre. Votre discussion doit donc être l'expression de votre curiosité pour tous les aspects de l'expérience. Elle doit aussi montrer votre maîtrise des outils d'analyse des résultats, tels les notions d'erreurs, d'écarts, d'incertitudes et la logique de votre travail.

\smallskip
Cependant, un bon point de départ pour votre discussion est de présenter vos résultats en français à l'aide de vos graphes.

\subsection{Analyse}
Après la présentation des résultats il convient d'en souligner tout ce qui peut être hors norme, inattendu, bizarre. Mieux serait de tenter de donner des explications plausibles à cet inattendu, voir encore mieux des explications vérifiées.

\subsection{Problèmes rencontrés}
Une critique constructive de votre expérience est enfin la bienvenue, car elle peut permettre à d'autres de ne pas reproduire vos erreurs.

\section{Conclusion}
Voici un exemple de conclusion. À éviter : mettre ici les remerciements.

\medskip
Les mesures effectuées montrent clairement que le seul paramètre qui fait varier la période d'un pendule est sa longueur. De plus, on a pu mettre en évidence une dépendance de la période en fonction de la racine de la longueur. Comme théoriquement, on peut montrer que~:
\[T=2\cdot \pi\cdot \sqrt{\frac{L}{g}}\]
et étant donné les imprécisions de mesure dues à l'utilisation du pouls pour mesurer la période, l'expérience à donné de bons résultats.

\newpage
\section{Annexes}
\appendix
Mettez en annexe tout ce que vous voulez conserver dans le cadre de ce travail pratique, mais qu'il vous semble inutile de lire. On peut penser pour le travail sur le pendule simple à une présentation de l'utilisation de la balance de précision, qui peut s'avérer utile par la suite, mais n'est pas nécessaire pour comprendre votre travail.

\section{Équations}
Plusieurs environnements pour réaliser des équations sont disponibles. Sans être exhaustif, peut les distinguer selon la nécessité d'écrire des équations en ligne ou hors ligne.

Par exemple, l'équation \(F=m\cdot a\) est une équation en ligne, alors que~: \[F=m\cdot a\] est hors ligne.

Ces deux équations s'écrivent~:
\begin{itemize}
\item pour l'équation en ligne \verb|\(F=m\cdot a\)| et
\item pour l'équation hors ligne \verb|\[F=m\cdot a\]|
\end{itemize}

\smallskip
Ces deux manières d'écrire ne permettent pas la numérotation des équations. Généralement, on ne numérote pas les équations en ligne, car si elles sont en ligne c'est qu'elles sont d'une importance moindre. Par contre, certaines équations hors ligne devant être numérotées, il existe un environnement spécifique pour cela : \emph{equation}. Pour écrire l'équation \ref{eq:labelleeq}~:
\begin{equation}\label{eq:labelleeq}
F=m\cdot a
\end{equation}
on doit écrire~:
\begin{verbatim}
\begin{equation}\label{eq:labelleeq}
F=m\cdot a
\end{equation}
\end{verbatim}
avec, grâce à la commande \verb|\label|, la possibilité d'évoquer le numéro de l'équation par \verb|\ref{eq:labelleeq}| dans le texte.

Relevons qu'il est possible de supprimer la numérotation de cet environnement en écrivant autant dans le begin que dans le end le mot equation avec une étoile, soit \verb|equation*|.

\medskip
Finalement, un environnement permettant d'écrire des équations alignées verticalement sur la base du caractère \&, est aussi disponible. Il s'agit d'\emph{align}. Ainsi, pour écrire les équations \ref{eq:alignnewton}, \ref{eq:alignhook} et suivante~:
\begin{align}
F&=m\cdot a\label{eq:alignnewton}\\
F&=G\cdot \frac{M\cdot m}{r^2}\label{eq:alignhook}\\
F&=k\cdot \frac{Q\cdot q}{r^2}\nonumber
\end{align}
on utilise~:
\begin{verbatim}
\begin{align}
F&=m\cdot a\label{eq:alignnewton}\\
F&=G\cdot \frac{M\cdot m}{r^2}\label{eq:alignhook}\\
F&=k\cdot \frac{Q\cdot q}{r^2}\nonumber
\end{align}
\end{verbatim}
avec la possibilité de supprimer la numérotation d'une ligne en lui apposant la commande \verb|\nonumber|.

Relevons enfin que \verb|align*| existe aussi pour supprimer toute numérotation.

\section{Schéma}\label{annexe:schema}
Les problèmes posés par les schémas sont nombreux. Essentiellement, il s'agit d'utiliser un logiciel de dessin vectoriel comme Inkscape, puis de l'utiliser dans \LaTeX. Or, comme on peut le voir à la figure \ref{figure:annexedispos}, l'échelle du schéma a une influence directe sur le texte qu'îl utilise et qui est souvent dans des proportions différentes du texte utilisé dans \LaTeX.

\begin{figure}
\centering
\includegraphics[scale=1.6]{RailHorizontal.eps} 
\caption{Le rail horizontal}\label{figure:annexedispos}
\end{figure}

Pour résoudre ce problème, il faut utiliser des fonctionnalités avancées d'Inkscape et du format eps. Le résultat est présenté dans la figure \ref{figure:annexedispos2}, montre que le texte est de bien meilleure qualité et même présente la possibilité d'utiliser des équations correctement affichées.

\medskip
Comment cela est-il possible ? Lors de l'enregistrement (\verb|Enregistrer une copie...|) d'un fichier svg créé sous Inkscape, puis enregistré au format eps, apparaît dans le menu du format eps la possibilité de séparer les éléments graphiques et le texte et de créer un fichier \LaTeX{} permettant l'import du fichier eps parallèlement au placement du texte sur celui-ci.

Concrètement, la figure \ref{figure:exporteps} présente la configuration de l'exportation eps à partir d'Inkscape. On y voit la condition~:
\begin{verbatim}
Exclure le texte du fichier PDF, et créer un fichier LaTeX
\end{verbatim}
qui va partager le schéma en un fichier postscript encapsulé (.eps) et un fichier \LaTeX{} (.eps\_tex) contenant respectivement les éléments graphiques et le texte.

\smallskip
L'inclusion du graphique ne se fait alors plus via la commande \verb|\includegraphics|, mais par~:
\begin{verbatim}
\begin{figure}[ht]
\caption[Rail]{Le rail horizontal}\label{figure:annexedispos2}
\smallskip
\centering
%\def\svgwidth{0.7\columnwidth}
\def\svgwidth{10cm}
\input{./images/RailHorizontal2.eps_tex}
\end{figure}
\end{verbatim}
Ainsi, on insère (\verb|\input|) le code \LaTeX{} qui va placer le schéma.

Il est possible d'intervenir dans le code \LaTeX\ créé par Inkscape pour changer le texte par exemple. N'hésitez pas à l'étudier.

\begin{figure}
\centering
\includegraphics[scale=0.5]{PostscriptEncapsule.eps} 
\caption{Export en eps-latex}\label{figure:exporteps}
\end{figure}

\begin{figure}[ht]
\caption[Rail]{Le rail horizontal}\label{figure:annexedispos2}
\smallskip
\centering
%\def\svgwidth{0.7\columnwidth}
\def\svgwidth{10cm}
\input{./images/RailHorizontal2.eps_tex}
\end{figure}

\medskip
Il faut cependant relever qu'il faut utiliser la condition~:
\begin{verbatim}
Utiliser la taille de l'objet exporté
\end{verbatim}
lors de l'exportation en eps et qu'il est impératif que le texte soit entièrement contenu dans le cadre des éléments graphique. S'il dépasse hors des limites du rectangle les englobants, le positionnement du texte devient incontrôlable. Si cela est impossible, on peut ne mettre qu'une lettre en lieu et place de chaque élément de texte, puis intervenir sur le code \LaTeX\ du fichier eps\_tex pour associer à chaque lettre le code \LaTeX\ nécessaire à la production des légendes. C'est la raison pour laquelle dans la figure \ref{figure:annexedispos2} le rail a été élargi vers le bas pour permettre un bon placement des légendes \(x_0\) et \(x\).

\begin{figure}[ht]
\caption[Rail]{Le rail horizontal}\label{figure:annexedispos3}
\smallskip
\centering
%\def\svgwidth{0.7\columnwidth}
\def\svgwidth{5cm}
\input{./images/RailHorizontal2.eps_tex}
\end{figure}

Relevez enfin que les changements d'échelle ont leur limites dans le cadre de cette méthode. En effet, le texte placé par \LaTeX\ conservant sa taille normale, une diminution de l'échelle des éléments graphiques va inévitablement produire des problèmes de chevauchement, comme le montre la figure \ref{figure:annexedispos3} où la taille de l'image à été imposée à \SI{5}{\centi\metre}.

Il est donc nécessaire lors de la création du ficher svg avec Inkscape de travailler sur une figure de la taille envisagée dans le document \LaTeX\, soit environ \SI{16}{\centi\metre} pour une page mono-colonne sur du papier A4 et \SI{8}{\cent

\section{Notes, références et flottants}
Utilisons cette deuxième annexe pour présenter divers éléments formels utilisable avec \LaTeX{} dans votre rapport.
\subsection{Notes}
Les notes de bas de page peuvent être utilisées de la manières suivante\footnote{Regardez dans le code du modèle.}.
\subsection{Références}
Dans un document tel qu'un travail de maturité, une bibliographie est indispensable. Si vous désirez en faire une pour votre rapport de physique, il faut tout d'abord simplement mettre une référence d'entrée de bibliographie telle que \cite{EinsteinPR1935} et mettre la description de l'ouvrage dans l'environnement \verb|thebibliography|, comme présenté dans le code source de ce document. Si vous avez moins de 10 références mettez en paramètre le chiffre 9 et 99 si vous en avez moins de cent. Il s'agit là d'une bibliographie très simple obéissant aux canons de la physique. Pour générer des bibliographies plus complexes, utilisez le programme externe \emph{bibtex}.

\subsection{Placement des flottants}
Techniquement, le placement des éléments graphiques flottants\footnote{Voir l'excellent site :\\\url{https://en.wikibooks.org/wiki/LaTeX/Floats,_Figures_and_Captions}} se fait par l'intermédiaire des environnements \verb|figure|, \verb|table|, \verb|subfigure| et \verb|subtable|, pour autant que le module \verb|subcaption| soit chargé.

Ce placement peut être influencé par les éléments :
\begin{description}
\item [h] pour placer l'élément ici, approximativement,
\item [t] pour placer l'élément en haut de page,
\item [b] pour placer l'élément en bas de page,
\item [p] pour placer l'élément sur une page de flottants,
\item [~!] pour tenter de forcer \LaTeX\ à faire ce qu'on veut et
\item [H] pour placer l'élément exactement ici (nécessite le paquel \verb|float|).
\end{description}
placés entre crochets de la manière suivante :
\begin{verbatim}
\begin{figure}[tbh...]
\end{verbatim}

\begin{thebibliography}{9}
\bibitem{EinsteinPR1935}A.~Einstein and N.~Rosen, Phys. Rev. \textbf{48}, 73 (1935)
\end{thebibliography}

\section{Gnuplot}\label{annexe:gnuplot}
L'utilisation de gnuplot, si elle n'est pas complexe, nécessite des connaissances de base pour obtenir des graphes de bonne qualité.

L'objectif de cette annexe est de présenter par deux exemples concrets la manière de réaliser des graphes à partir de données chiffrés. Elle est généralement celle utilisée par la physique, puisque la plupart des graphes dans ce domaine se basent sur des mesures.

L'utilisation de siunitx (du métapaquet texlive-science) est intéressante, car elle permet d'utiliser le module des unités dans gnuplot. Elle est activée en paramètre de l'utilisation du package gnuplottex qui permet lui d'utiliser gnuplot directement dans \LaTeX.

Les données sont celles de la table \ref{table:donnees}.
\begin{table}
\centering
\begin{tabular}{lllll}
1	& 1	& 0.1	& 1	& 2 \\
2	& 4	& 0.4	& 2	& 4 \\
3	& 11 &	0.2 &	3 &	6 \\
4	& 18 &	0.5 &	4 &	8 \\
5	& 26 &	0.3 &	5 &	10 \\
6	& 31 &	0.1 &	6 &	12 \\
&&&&\\
&&&&\\
10 &	16 &	5 &&\\
20 &	45 &	10 &&\\
30 &	53 &	15 &&\\
40 &	89 &	20 &&\\
50 &	110 &	25 &&\\
60 &	135 &	30 &&\\
70 &	140 &	35 &&\\
80 &	155 &	40 &&\\
90 &	170 &	45 &&\\
100 &	200 &	50 &&
\end{tabular}
\caption{Les donnée séparées par des tabulations}\label{table:donnees}
\end{table}

\begin{figure}[h]
\centering
\begin{gnuplot}[terminal=epslatex, terminaloptions=color dashed, scale=0.9]
# Le terminal epslatex (excellent) est utilisé pour faire le graphe
# on utilise des couleurs et éventuellement des traitillés
# on diminue légèrement la taille du graphe par rapport à la pleine page
# ---

# Déclaration du titre
# ---
# ici désactivé car c'est LaTeX qui le fait

#set title 'Équivalent Joule-Calorie\\ \small Une belle expérience'

# Placement de la légende (key)
# ---
# avec un cadre (box)
# à gauche (left), aligné à gauche (Left) avec un espace interligne de 1,5

set key box left Left spacing 1.5

# Création du fit des données
# ---
# la fonction de fit

f(x)=a*x**2+b*x

# l'index 0 indique que les données sont dans le premier tableau du fichier
# on utilise (u) les deux premières colonnes et les paramètres de fit sont a et b

fit f(x) 'test.txt' index 0 u 1:2 via a,b

# Placement des légendes des axes
# ---
# le rotate est là pour montrer qu'on peut éventuellement tourner les légendes
# remarquez l'utilisation de siunitx via \si

set ylabel '\rotatebox{0}{$\Delta \theta$ (\si{\celsius})}'
set xlabel 'Nombre de tours N'

# Tracé des graphes
# ---
# les intervalles sur x et y sont spécifiés en premier [...][...]
# le fichier des données est spécifié (test.txt)
# le premier bloc de données est utilisé (index 0)
# les quatre premières colonnes sont utilisées u 1:2:3:4
# le titre de la légende est spécifié title ...
# les barres d'erreurs sont demandées (par défaut il s'agit des colonnes 3 et 4
# ---
# puis un deuxième graphe est demandé sur la base des même données test.txt
# utilisant pour x la première colonne et pour y la cinquième
# noté dans la légende "Une droite"
# ---
# puis le fit des données est demandé comme troisième graphe avec la fonction f(x)
# légendé "Regres. polynom."
# ---
# remarquez l'utilisation d'un backslash (\) pour passer à la ligne et les
# abréviations comme u, t, w et lc pour using, title, with et linecolor

plot [0:7][0:45]\
 'test.txt' index 0 u 1:2:3:4 t 'Une puissance' w xyerrorbars,\
 'test.txt' index 0 using 1:5 title 'Une droite' linecolor 'red',\
 f(x) title 'Régres. polynom.' lc 'blue'
\end{gnuplot}
\caption{Le titre du premier graphe}\label{figure:premiergraphe}
\end{figure}

\medskip
Le code utilisé (et commenté pour le comprendre) pour produire le premier graphe (figure \ref{figure:premiergraphe}) est le suivant :
\begin{verbatim}
\begin{figure}[h]
\centering
\begin{gnuplot}[terminal=epslatex, terminaloptions=color dashed,
 scale=0.9]
# Le terminal epslatex (excellent) est utilisé pour faire le
 graphe
# on utilise des couleurs et éventuellement des traitillés
# on diminue légèrement la taille du graphe par rapport à
 la pleine page
# ---

# Déclaration du titre
# ---
# ici désactivé car c'est LaTeX qui le fait

#set title 'Équivalent Joule-Calorie\\ \small Une belle
 expérience'

# Placement de la légende (key)
# ---
# avec un cadre (box)
# à gauche (left), aligné à gauche (Left) avec un espace
 interligne de 1,5

set key box left Left spacing 1.5

# Création du fit des données
# ---
# la fonction de fit

f(x)=a*x**2+b*x

# l'index 0 indique que les données sont dans le premier
 tableau du fichier
# on utilise (u) les deux premières colonnes et les
 paramètres de fit sont a et b

fit f(x) 'test.txt' index 0 u 1:2 via a,b

# Placement des légendes des axes
# ---
# le rotate est là pour montrer qu'on peut éventuellement
 tourner les légendes
# remarquez l'utilisation de siunitx via \si

set ylabel '\rotatebox{0}{$\Delta \theta$ (\si{\celsius})}'
set xlabel 'Nombre de tours N'

# Tracé des graphes
# ---
# les intervalles sur x et y sont spécifiés en premier
 [...][...]
# le fichier des données est spécifié (test.txt)
# le premier bloc de données est utilisé (index 0)
# les quatre premières colonnes sont utilisées u 1:2:3:4
# le titre de la légende est spécifié title ...
# les barres d'erreurs sont demandées (par défaut il s'agit
 des colonnes 3 et 4
# ---
# puis un deuxième graphe est demandé sur la base des même
 données test.txt
# utilisant pour x la première colonne et pour y la cinquième
# noté dans la légende "Une droite"
# ---
# puis le fit des données est demandé comme troisième graphe
 avec la fonction f(x)
# légendé "Regres. polynom."
# ---
# remarquez l'utilisation d'un backslash (\) pour passer à
 la ligne et les
# abréviations comme u, t, w et lc pour using, title, with
 et linecolor

plot [0:7][0:45]\
 'test.txt' index 0 u 1:2:3:4 t 'Une puissance' w xyerrorbars,\
 'test.txt' index 0 using 1:5 title 'Une droite' linecolor
  'red',\
 f(x) title 'Régres. polynom.' lc 'blue'
\end{gnuplot}
\caption{Le titre du premier graphe}
\end{figure}
\end{verbatim}

\begin{figure}[h]
\begin{gnuplot}[terminal=epslatex, terminaloptions=color dashed]
# Placement de la légende

set key Left bottom

# Placement des légendes des axes

set ylabel 'Accélération a (\si{\metre\per\second\squared})'
set xlabel 'Nombre de tours N'

# Placement de la flèche et du texte

set arrow 1 from 50,175 to 58,140
set label 'Un point particulier sur $x=2\cdot x^2$' at 10,185

# Création du graphe
# ---
# on utilise le second bloc de données (index 1)
# la colonne 3 est celle des erreurs sur y
# on utilise le point numéro 4 (un petit carré) pour positionner la mesure

plot [0:110][0:250] 'test.txt'\
 index 1 u 1:2:3 title 'Second bloc' w yerrorbars pt 4
\end{gnuplot}
\caption{Le titre du second graphe}\label{figure:secondgraphe}
\end{figure}

\medskip
Le code utilisé (et commenté pour le comprendre) pour produire le second graphe (figure \ref{figure:secondgraphe}) est le suivant :
\begin{verbatim}
\begin{figure}[h]
\begin{gnuplot}[terminal=epslatex, terminaloptions=color
 dashed]
# Placement de la légende

set key Left bottom

# Placement des légendes des axes

set ylabel 'Accélération a (\si{\metre\per\second\squared})'
set xlabel 'Nombre de tours N'

# Placement de la flèche et du texte

set arrow 1 from 50,175 to 58,140
set label 'Un point particulier sur $x=2\cdot x^2$' at 10,185

# Création du graphe
# ---
# on utilise le second bloc de données (index 1)
# la colonne 3 est celle des erreurs sur y
# on utilise le point numéro 4 (un petit carré) pour
 positionner la mesure

plot [0:110][0:250] 'test.txt'\
 index 1 u 1:2:3 title 'Second bloc' w yerrorbars pt 4
\end{gnuplot}
\caption{Le titre du second graphe}
\end{figure}
\end{verbatim}

\end{document}