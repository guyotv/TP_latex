set terminal epslatex color dashed
set output 'ModeleLaTeX_TP-gnuplottex-fig2.tex'
set format '\num{%g}'
# Le terminal epslatex (excellent) est utilisé pour faire le graphe
# on utilise des couleurs et éventuellement des traitillés
# on diminue légèrement la taille du graphe par rapport à la pleine page
# ---

# Déclaration du titre
# ---
# ici désactivé car c'est LaTeX qui le fait

#set title 'Équivalent Joule-Calorie\\ \small Une belle expérience'

# Placement de la légende (key)
# ---
# avec un cadre (box)
# à gauche (left), aligné à gauche (Left) avec un espace interligne de 1,5

set key box left Left spacing 1.5

# Création du fit des données
# ---
# la fonction de fit

f(x)=a*x**2+b*x

# l'index 0 indique que les données sont dans le premier tableau du fichier
# on utilise (u) les deux premières colonnes et les paramètres de fit sont a et b

fit f(x) 'test.txt' index 0 u 1:2 via a,b

# Placement des légendes des axes
# ---
# le rotate est là pour montrer qu'on peut éventuellement tourner les légendes
# remarquez l'utilisation de siunitx via \si

set ylabel '\rotatebox{0}{$\Delta \theta$ (\si{\celsius})}'
set xlabel 'Nombre de tours N'

# Tracé des graphes
# ---
# les intervalles sur x et y sont spécifiés en premier [...][...]
# le fichier des données est spécifié (test.txt)
# le premier bloc de données est utilisé (index 0)
# les quatre premières colonnes sont utilisées u 1:2:3:4
# le titre de la légende est spécifié title ...
# les barres d'erreurs sont demandées (par défaut il s'agit des colonnes 3 et 4
# ---
# puis un deuxième graphe est demandé sur la base des même données test.txt
# utilisant pour x la première colonne et pour y la cinquième
# noté dans la légende "Une droite"
# ---
# puis le fit des données est demandé comme troisième graphe avec la fonction f(x)
# légendé "Regres. polynom."
# ---
# remarquez l'utilisation d'un backslash (\) pour passer à la ligne et les
# abréviations comme u, t, w et lc pour using, title, with et linecolor

plot [0:7][0:45]\
 'test.txt' index 0 u 1:2:3:4 t 'Une puissance' w xyerrorbars,\
 'test.txt' index 0 using 1:5 title 'Une droite' linecolor 'red',\
 f(x) title 'Régres. polynom.' lc 'blue'
