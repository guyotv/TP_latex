set terminal epslatex color dashed
set output 'ModeleLaTeX_TP-gnuplottex-fig1.tex'
set format '\num{%g}'
set key bottom left Left
set key width 1.5
set sample 1000
set xr [0:110]
set yr [0:20]
set xlabel 'Masse (\si{\gram})' # attention il faut parfois utiliser certains guillemets
set ylabel "Période (puls)"
#plot test.txt w l lc 1 t "$\sin(x)$",cos(x) w l lc 2 t "$\cos(x)$",tan(x) w l lc 3 t "$\tan(x)$",tanh(x) w l lc 4 t "$\tanh(x)$"
plot 'test.txt'\
 index 2 u 1:2:3 title 'Petites masses' w yerrorbars pt 4
