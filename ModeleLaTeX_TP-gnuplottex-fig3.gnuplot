set terminal epslatex color dashed
set output 'ModeleLaTeX_TP-gnuplottex-fig3.tex'
set format '\num{%g}'
# Placement de la légende

set key Left bottom

# Placement des légendes des axes

set ylabel 'Accélération a (\si{\metre\per\second\squared})'
set xlabel 'Nombre de tours N'

# Placement de la flèche et du texte

set arrow 1 from 50,175 to 58,140
set label 'Un point particulier sur $x=2\cdot x^2$' at 10,185

# Création du graphe
# ---
# on utilise le second bloc de données (index 1)
# la colonne 3 est celle des erreurs sur y
# on utilise le point numéro 4 (un petit carré) pour positionner la mesure

plot [0:110][0:250] 'test.txt'\
 index 1 u 1:2:3 title 'Second bloc' w yerrorbars pt 4
